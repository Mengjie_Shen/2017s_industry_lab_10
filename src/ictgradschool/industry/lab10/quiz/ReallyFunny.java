package ictgradschool.industry.lab10.quiz;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;


/**
 * Created by mshe666 on 5/12/2017.
 */
public class ReallyFunny {
    public void start() {

        File myFile = new File("infile2.txt");
        Map<String, String> weightMap = new HashMap<String, String>();

        try (Scanner scanner = new Scanner(new FileReader(myFile))) {
            String splitter = ",";
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] lineArray = line.split(splitter);
                ArrayList<String> arrayList = new ArrayList<>();
                for (String s : lineArray) {
                    arrayList.add(s);
                }
                String name = lineArray[0];
                String weight = lineArray[1];

                if (weightMap.get(name) == null) {
                    weightMap.put(name, weight);
                }
            }
        }catch (IOException e) {
            System.out.println(e.getStackTrace());
        }

        for (String key : weightMap.keySet()) {
            System.out.println(key + "\t" + weightMap.get(key));
        }

    }

    public static void main(String[] args) {
        ReallyFunny f = new ReallyFunny();
        f.start();
    }
}
