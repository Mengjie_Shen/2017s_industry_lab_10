package ictgradschool.industry.lab10.quiz;


import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by mshe666 on 5/12/2017.
 */
public class Funny {
    public void start() {

        File myFile = new File("infile.txt");
        ArrayList<String> arrayList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(myFile))) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                arrayList.addAll(Arrays.asList(line.split(" ")));
            }
        }catch (IOException e) {
            System.out.println(e.getStackTrace());
        }

        for (String s : arrayList) {
            System.out.println(s);
        }

        Collections.shuffle(arrayList);
        System.out.println("------------------");

        for (String s : arrayList) {
            System.out.println(s);
        }

    }

    public static void main(String[] args) {
        Funny f = new Funny();
        f.start();
    }


}
