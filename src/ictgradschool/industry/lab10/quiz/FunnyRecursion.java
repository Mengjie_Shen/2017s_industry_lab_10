package ictgradschool.industry.lab10.quiz;

/**
 * Created by mshe666 on 5/12/2017.
 */
public class FunnyRecursion {

    public void start() {

        System.out.println("power(" + 3 + "," + 4 + ") = " + power(3, 4));
        System.out.println("power(" + 2 + "," + 10 + ") = " + power(2, 10));
        System.out.println("power(" + 1 + "," + 3 + ") = " + power(1, 3));
        System.out.println("power(" + 8 + "," + 0 + ") = " + power(8, 0));

        String str1 = "qwertyuiop";
        System.out.println(str1);
        System.out.println(reverseString(str1));

    }

    public int power(int x, int y) {
        if (y == 0) {
            return 1;
        }
        else if (y == 1) {
            return x;
        }
        return x * power(x, y - 1);
    }

    public String reverseString(String str) {
        if (str.length() == 0) {
            return "";
        }
        return reverseString(str.substring(1)) + str.charAt(0);
    }

    public static void main(String[] args) {
        FunnyRecursion f = new FunnyRecursion();
        f.start();
    }
}
